﻿using Common.Core.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SALE_CAFE
{
    public partial class Frm_Product_Add : Form
    {
        public Frm_Product_Add()
        {
            InitializeComponent();
        }

        int prId = 0 ;
        private void btnAdd_Click(object sender, EventArgs e)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    if (txtPrice.Text != "" && txtProductName.Text != "" && txtQuantity.Text != "")
                    {
                        DataTable dt = db.SqlQueryWithResult("[dbo].[PRODUCT_INSERT]", CommandType.StoredProcedure,
                            new SqlParameter("@name", txtProductName.Text),
                            new SqlParameter("@quantity", int.Parse(txtQuantity.Text)),
                            new SqlParameter("@price", float.Parse(txtPrice.Text)));
                        if (dt.Rows[0]["ErrorCode"].ToString() == "1")
                        {
                            Clear();
                            MessageBox.Show(dt.Rows[0]["ErrorMessage"].ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }
                        else
                        {
                            MessageBox.Show("Lỗi hệ thông !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Bạn vui lòng nhập thông tin !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Lỗi");
                }
            }
        }
        private void Clear()
        {
            txtPrice.Text = "";
            txtProductName.Text = "";
            txtQuantity.Text = "";
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void gvProduct_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            var row = gvProduct.GetDataRow(e.RowHandle);
            prId = int.Parse(row["PRODUCTID"].ToString());
        }

        private void Frm_Product_Add_Load(object sender, EventArgs e)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    DataTable dt = db.SqlQueryWithResult("[dbo].[PRODUCT_GETPRODUCT]", CommandType.StoredProcedure);
                    if (dt.Rows.Count > 0)
                    {
                        grProduct.DataSource = dt;
                    }
                    else
                    {
                        grProduct.DataSource = null;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Lỗi");
                }
            }
        }

        private void btnTrash_Click(object sender, EventArgs e)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    if (MessageBox.Show("Bạn có thật sự muốn xóa !", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        db.SqlQuery("[dbo].[PRODUCT_UPDATE_STATUS]", CommandType.StoredProcedure,
                        new SqlParameter("@prId", prId));
                    }
                    Frm_Product_Add_Load(sender, e);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Lỗi");
                }
            }
        }
    }
}
