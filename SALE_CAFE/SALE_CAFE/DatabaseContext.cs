﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace Common.Core.Data
{    
    public class DatabaseContext : IDisposable
    {
        private SqlConnection _sqlConnection;
        private string _connectionString = string.Empty;

        public void ReadConnectionString()
        {
           
            string Server = "TANNGOC";
            string DataSource = "MANAGE_SALE";
            string Uid = "sa";
            string Pwd = "123456";
            //Data Source=TANNGOC;Initial Catalog=MANAGE_SALE;User ID=sa;Password=***********
            _connectionString = "server=" + Server + ";database=" + DataSource + ";uid=" + Uid + ";pwd=" + Pwd + ";";
        }

        public DatabaseContext()
        {
            beginTransaction();
        }

        public DatabaseContext(string consStr)
        {
            _connectionString = consStr;
            beginTransaction();
        }

        public bool IsConnected
        {
            get
            {
                return checkIsConnected();
            }
            private set { }
        }

        private bool checkIsConnected()
        {
            if (_connectionString == string.Empty)
            {
                ReadConnectionString();
            }

            SqlConnection conn = new SqlConnection(_connectionString);
            try
            {                
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();                                        
                    return true;
                }                
            }
            catch (Exception)
            { 
            
            }
            finally 
            {
                conn.Close();
            }
            return false;
        }

        private void beginTransaction()
        {
            if (checkIsConnected())
            {
                _sqlConnection = new SqlConnection(_connectionString);
                _sqlConnection.Open();
            }        
        }

        public bool SqlQuery(string sql, CommandType ct, params SqlParameter[] param)
        {
            bool result = false;
            using (SqlTransaction _sqlTransaction = _sqlConnection.BeginTransaction())
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(sql, _sqlConnection, _sqlTransaction);
                    cmd.CommandType = ct;
                    cmd.CommandTimeout = 6000;
                    cmd.Parameters.Clear();
                    if (param != null)
                    {
                        foreach (SqlParameter p in param)
                        {
                            cmd.Parameters.Add(p);
                        }
                    }

                    cmd.ExecuteNonQuery();
                    _sqlTransaction.Commit();
                    result = true;
                }
                catch (Exception ex)
                {
                    _sqlTransaction.Rollback();
                    Console.WriteLine(ex.Message);
                    result = false;
                }
            }
            return result;
        }

        public bool SqlQuery(string sql, CommandType ct, ref string err, params SqlParameter[] param)
        {
            bool result = false;
            using (SqlTransaction _sqlTransaction = _sqlConnection.BeginTransaction())
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(sql, _sqlConnection, _sqlTransaction);
                    cmd.CommandType = ct;
                    cmd.CommandTimeout = 6000;
                    cmd.Parameters.Clear();
                    if (param != null)
                    {
                        foreach (SqlParameter p in param)
                        {
                            cmd.Parameters.Add(p);
                        }
                    }

                    cmd.ExecuteNonQuery();
                    _sqlTransaction.Commit();
                    result = true;
                }
                catch (Exception ex)
                {
                    _sqlTransaction.Rollback();
                    err = ex.Message;
                    Console.WriteLine(ex.Message);
                    result = false;
                }
            }
            return result;
        }

        public DataTable SqlQueryWithResult(string sql, CommandType ct, ref string err, params SqlParameter[] param)
        {
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand(sql, _sqlConnection);
                cmd.CommandType = ct;
                cmd.CommandTimeout = 6000;
                cmd.Parameters.Clear();
                if (param != null)
                {
                    foreach (SqlParameter p in param)
                        cmd.Parameters.Add(p);
                }

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (SqlException ex)
            {
                err = ex.Message;
                Console.WriteLine(ex.Message);
            }

            return dt;
        }

        public DataTable SqlQueryWithResult(string sql, CommandType ct, params SqlParameter[] param)
        {
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand(sql, _sqlConnection);
                cmd.CommandType = ct;
                cmd.CommandTimeout = 6000;
                cmd.Parameters.Clear();
                if (param != null)
                {
                    foreach (SqlParameter p in param)
                        cmd.Parameters.Add(p);
                }

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return dt;
        }

        public T SqlQueryGetValue<T>(string sql, CommandType ct, params SqlParameter[] param) where T : class
        {
            DataTable dt = new DataTable();
            var result = new object();

            try
            {
                SqlCommand cmd = new SqlCommand(sql, _sqlConnection);
                cmd.CommandType = ct;
                cmd.CommandTimeout = 6000;
                cmd.Parameters.Clear();
                if (param != null)
                {
                    foreach (SqlParameter p in param)
                        cmd.Parameters.Add(p);
                }

                result = cmd.ExecuteScalar();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return (T)Convert.ChangeType(result, typeof(T));
        }

        public SqlDataAdapter SqlQueryWithAdapter(string sql, CommandType ct, params SqlParameter[] param)
        {
            SqlDataAdapter da = null;
            try
            {
                SqlCommand cmd = new SqlCommand(sql, _sqlConnection);
                cmd.CommandType = ct;
                cmd.CommandTimeout = 6000;
                cmd.Parameters.Clear();
                if (param != null)
                {
                    foreach (SqlParameter p in param)
                        cmd.Parameters.Add(p);
                }

                da = new SqlDataAdapter(cmd);                
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return da;
        }

        public void Dispose()
        {
            if (_sqlConnection != null)
            {
                if (_sqlConnection.State == ConnectionState.Open)
                    _sqlConnection.Close();
                _sqlConnection.Dispose();
            }
        }

      
    }
}
