﻿using Common.Core.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SALE_CAFE
{
    public partial class Frm_SaleCafe : Form
    {
        string tbId = string.Empty;
        DataTable dt_listProduct ;
        public Frm_SaleCafe()
        {
            InitializeComponent();
        }
        public void Frm_SaleCafe_Load(object sender, EventArgs e)
        {
            flowLayoutPanel1.Controls.Clear();
            LoadTable();
            Clear();
        }
        private void Clear()
        {
            tbId = "0";
            btnOpenTable.Text = "Mở bàn";
            txtCustomerPay.Text = "0";
            txtSumMoney.Text = "0";
            txtRefunds.Text = "0";
            grListProduct.DataSource = null;
            dt_listProduct = new DataTable();
        }
        private void LoadTable()
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    DataTable dt = db.SqlQueryWithResult("[dbo].[TABLE_GETALL]", CommandType.StoredProcedure);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            string tbId = dr["TABLEID"].ToString();
                            string name = dr["TABLENAME"].ToString();
                            string color = string.Empty;
                            if ((bool)dr["STATUS"] == true) { color = "Red"; } else { color = "Green"; };
                            flowLayoutPanel1.Controls.Add(Panel_add(button(tbId, name, color), picture(tbId)));
                        }
                    }
                    else
                    {
                        MessageBox.Show("Vui lòng tạo bàn !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Frm_Table_Add form = new Frm_Table_Add();
                        form.ShowDialog();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Lỗi");
                }
            }
        }
        private Panel Panel_add(Button btn, PictureBox pic)
        {
            Panel pnl = new Panel();
            pnl.Controls.Add(btn);
            pnl.Controls.Add(pic);
            pnl.Size = new Size(93, 149);
            return pnl;
        }
        private Button button(string tbId, string name, string color)
        {
            Button btn = new Button();
            btn.Cursor = Cursors.Hand;
            btn.Dock = DockStyle.Bottom;
            btn.Name = tbId;
            btn.Text = name;
            btn.Size = new Size(93, 53);
            btn.Click += new EventHandler(btnClick);
            btn.Leave += new EventHandler(btnLeave);
            btn.BackColor = Color.FromName(color);
            return btn;
        }

        private PictureBox picture(string tbId)
        {
            PictureBox pic = new PictureBox();
            pic.Cursor = Cursors.Hand;
            pic.Dock = DockStyle.Top;
            pic.Image = Properties.Resources.cafe;
            pic.Name = tbId;
            pic.SizeMode = PictureBoxSizeMode.StretchImage;
            pic.Size = new Size(93, 96);
            return pic;
        }
        private void btnClick(object sender, EventArgs e)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    tbId = (sender as Button).Name;

                    if (CheckTable(tbId) == true)
                    {
                        dt_listProduct = db.SqlQueryWithResult("[dbo].[BILL_TABLE_GETALL]", CommandType.StoredProcedure,
                            new SqlParameter("@tbId", tbId));
                        if (dt_listProduct.Rows.Count > 0)
                        {
                            float sum_money = float.Parse(dt_listProduct.Compute("SUM(MONEY)", string.Empty).ToString());
                            txtSumMoney.Text = string.Format("{0:#,###}", sum_money);
                            grListProduct.DataSource = dt_listProduct;
                        }
                    }
                    else
                    {
                        Clear();
                        (sender as Button).BackColor = Color.Red;
                        (sender as Button).ForeColor = Color.White;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Lỗi");
                }
            }
                tbId = (sender as Button).Name;
           
        }
        private void btnLeave(object sender, EventArgs e)
        {
            tbId = (sender as Button).Name;

            if (CheckTable(tbId) == false)
            {
                (sender as Button).BackColor = Color.Green;
                (sender as Button).ForeColor = Color.Black;
            }

        }

        private bool CheckTable(string tbId)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    DataTable dt = db.SqlQueryWithResult("[dbo].[TABLE_GETALL]", CommandType.StoredProcedure,
                         new SqlParameter("@tbId", tbId));

                    if ((bool)dt.Rows[0]["STATUS"] == true)
                    {
                        return true; // open table 
                    }
                    return false; // close table
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Lỗi");
                    return false;
                }
            }

        }
        private void btnOpenTable_Click(object sender, EventArgs e)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    if (btnOpenTable.Text == "Mở bàn")
                    {
                        if (CheckTable(tbId) == false)
                        {
                            btnOpenTable.Text = "Gửi Order";

                            DataTable dt = db.SqlQueryWithResult("[dbo].[PRODUCT_GETPRODUCT]", CommandType.StoredProcedure);
                            if (dt.Rows.Count > 0)
                            {
                                flowLayoutPanel1.Controls.Clear();
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    string productId = dt.Rows[i]["PRODUCTID"].ToString();
                                    string name = dt.Rows[i]["PRODUCTNAME"].ToString();
                                    float price = float.Parse(dt.Rows[i]["PRICE"].ToString());

                                    Button btn = new Button();
                                    btn.Name = productId;
                                    btn.Text = string.Concat(name, "\n", "Giá bán :", string.Format("{0:#,###}", price));
                                    btn.Size = new Size(135, 140);
                                    btn.Font = new Font("Tahoma", 12F);
                                    btn.Click += new EventHandler(btnOrder_Click);
                                    flowLayoutPanel1.Controls.Add(btn);
                                }
                            }
                            else
                            {
                                MessageBox.Show("Bạn vui lòng thêm sản phẩm !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                Frm_Product_Add form = new Frm_Product_Add();
                                form.ShowDialog();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Bàn đang sử dụng, vui lòng thanh toán để mở bàn mới !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                    else
                    {

                        // update open table 
                        db.SqlQuery("[dbo].[TABLE_UPDATE_TRUE]", CommandType.StoredProcedure,
                            new SqlParameter("@tbId", tbId));
                        // insert bill
                        DataTable dt_bill = db.SqlQueryWithResult("[dbo].[BILL_INSERT]", CommandType.StoredProcedure,
                            new SqlParameter("@tbId", tbId),
                            new SqlParameter("@sum_money", float.Parse(txtSumMoney.Text)));

                        if (!string.IsNullOrEmpty(dt_bill.Rows[0]["BILLID"].ToString()))
                        {
                            string billId = dt_bill.Rows[0]["BILLID"].ToString();

                            foreach (DataRow dr in dt_listProduct.Rows)
                            {
                                string prId = dr["PRODUCTID"].ToString();
                                string quan = dr["QUANTITY"].ToString();
                                string price = dr["PRICE"].ToString();
                                string money = dr["MONEY"].ToString();
                                string sum_quan = dr["SUM_QUANTITY"].ToString();

                                // insert bill detail
                                DataTable dt_detail = db.SqlQueryWithResult("[dbo].[BILL_DETAIL_INSERT]", CommandType.StoredProcedure,
                                    new SqlParameter("@billId", billId),
                                    new SqlParameter("@prId", prId),
                                    new SqlParameter("@quantity", quan),
                                    new SqlParameter("@price", price),
                                    new SqlParameter("@money", money));

                                // update quantity in table product

                                quan = (int.Parse(sum_quan) - int.Parse(quan)).ToString();

                                db.SqlQueryWithResult("[dbo].[PRODUCT_UPDATE_QUANTITY]", CommandType.StoredProcedure,
                                    new SqlParameter("@prId", prId),
                                    new SqlParameter("@quan", quan));
                            }

                        }

                        //retun main table
                        flowLayoutPanel1.Controls.Clear();
                        btnOpenTable.Text = "Mở bàn";
                        LoadTable();
                        Clear();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Lỗi");
                }
            }
        }

        private void btnInsertProduct_Click(object sender, EventArgs e)
        {
            Frm_Product_Add form = new Frm_Product_Add();
            form.ShowDialog();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnInsertTable_Click(object sender, EventArgs e)
        {
            Frm_Table_Add form = new Frm_Table_Add();
            form.ShowDialog();
        }
        private void btnOrder_Click(object sender, EventArgs e)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    string prId = (sender as Button).Name.ToString();
                    int row_number = 1;
                    int quantity = 0;
                    float price = 0;
                    float money = 0;
                    DataTable dt = db.SqlQueryWithResult("[dbo].[PRODUCT_GETPRODUCT]", CommandType.StoredProcedure,
                        new SqlParameter("@prId", prId));
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            string prName = dr["PRODUCTNAME"].ToString();
                            string sum_quan = dr["QUANTITY"].ToString();
                            price = float.Parse(dr["PRICE"].ToString());
                            quantity = 1;
                            money = quantity * price;

                            if (dt_listProduct.Columns.Count < 1)
                            {
                                dt_listProduct.Columns.Add("ROW_NUMBER", typeof(int));
                                dt_listProduct.Columns.Add("PRODUCTID", typeof(int));
                                dt_listProduct.Columns.Add("PRODUCTNAME", typeof(string));
                                dt_listProduct.Columns.Add("SUM_QUANTITY", typeof(int));
                                dt_listProduct.Columns.Add("QUANTITY", typeof(int));
                                dt_listProduct.Columns.Add("PRICE", typeof(float));
                                dt_listProduct.Columns.Add("MONEY", typeof(float));

                                dt_listProduct.Rows.Add(new object[] { row_number, prId, prName, sum_quan, quantity, price, money });
                            }
                            else
                            {
                                DataRow[] product = dt_listProduct.Select("ProductName = '" + prName + "'");
                                if (product.Length > 0)
                                {
                                    foreach (DataRow dr_pr in dt_listProduct.Rows)
                                    {
                                        if (dr_pr["PRODUCTNAME"].ToString() == prName)
                                        {
                                            dr_pr["QUANTITY"] = quantity = int.Parse(dr_pr["Quantity"].ToString()) + 1;
                                            dr_pr["MONEY"] = money = quantity * price;
                                        }
                                    }
                                }
                                else
                                {
                                    row_number++;
                                    dt_listProduct.Rows.Add(new object[] { row_number, prId, prName, sum_quan, quantity, price, money });
                                }
                            }
                        }
                        grListProduct.DataSource = dt_listProduct;
                        float sum_money = float.Parse(dt_listProduct.Compute("SUM(MONEY)", string.Empty).ToString());
                        txtSumMoney.Text = string.Format("{0:#,###}", sum_money);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Lỗi");
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    Frm_SaleCafe_Load(sender, e);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Lỗi");
                }
            }
        }

        private void btnTrashTable_Click(object sender, EventArgs e)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    if (CheckTable(tbId) == true) // open table
                    {
                        MessageBox.Show("Bàn này đang sử dụng, bạn không được phép xóa !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        if (MessageBox.Show("Bạn có thật sự mún xóa bàn này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            db.SqlQuery("[dbo].[TABLE_DELETE]", CommandType.StoredProcedure,
                            new SqlParameter("@tbId", tbId));
                        }
                    }
                    Frm_SaleCafe_Load(sender, e);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Lỗi");
                }
            }
        }

        private void btnPayment_Click(object sender, EventArgs e)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    if (CheckTable(tbId) == false) // close table
                    {
                        MessageBox.Show("Thanh toán không thành công !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        string[] total = txtSumMoney.Text.Split(',');
                        string[] refund = txtRefunds.Text.Split(',');
                        string[] cus = txtCustomerPay.Text.Split(',');

                        string sumMoney = string.Empty;
                        string refunds = string.Empty;
                        string cusPay = string.Empty;


                        for (int i = 0; i < total.Length; i++)
                        {
                            sumMoney += total[i].ToString();
                        }

                        for (int i = 0; i < refund.Length; i++)
                        {
                            refunds += refund[i].ToString();
                        }

                        for (int i = 0; i < cus.Length; i++)
                        {
                            cusPay += cus[i].ToString();
                        }

                        if (float.Parse(refunds) < 0 || float.Parse(txtCustomerPay.Text) == 0)
                        {
                            MessageBox.Show("Khách hàng vẫn chưa trả đủ tiền !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            DataTable dt = db.SqlQueryWithResult("[dbo].[BILL_TABLE_GETALL]", CommandType.StoredProcedure,
                                new SqlParameter("@tbId", tbId));
                            if (dt.Rows.Count > 0)
                            {
                                int billId = int.Parse(dt.Rows[0]["BILLID"].ToString());
                                db.SqlQuery("[dbo].[BILL_UPDATE_ISPAYMENT]", CommandType.StoredProcedure,
                                    new SqlParameter("@billId", billId)); // update is payment = true
                                db.SqlQuery("[dbo].[TABLE_UPDATE_FALSE]", CommandType.StoredProcedure,
                                    new SqlParameter("@tbId", tbId)); // update close table
                            }
                        }
                    }
                    Frm_SaleCafe_Load(sender, e);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Lỗi");
                }
            }
        }

        private void txtCustomerPay_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtCustomerPay.Text) && !string.IsNullOrWhiteSpace(txtRefunds.Text))
            {
                string[] total = txtSumMoney.Text.Split(',');
                string[] refund = txtRefunds.Text.Split(',');
                string[] cus = txtCustomerPay.Text.Split(',');

                string sumMoney = string.Empty;
                string refunds = string.Empty;
                string cusPay = string.Empty;

               
                for (int i=0; i < total.Length; i++)
                {
                    sumMoney += total[i].ToString(); 
                }

                for (int i = 0; i < refund.Length; i++)
                {
                    refunds += refund[i].ToString();
                }

                for (int i = 0; i < cus.Length; i++)
                {
                    cusPay += cus[i].ToString();
                }

                float result = float.Parse(cusPay) - float.Parse(sumMoney);

                txtCustomerPay.Text = string.Format("{0:#,###}", float.Parse(cusPay));
                txtRefunds.Text = string.Format("{0:#,###}", result);

                if (string.IsNullOrEmpty(txtRefunds.Text)) { txtRefunds.Text = "0"; }
            }
        }
    }
}
