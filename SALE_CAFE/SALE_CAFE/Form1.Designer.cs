﻿namespace SALE_CAFE
{
    partial class Frm_SaleCafe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.grListProduct = new DevExpress.XtraGrid.GridControl();
            this.gvListProduct = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ROW_NUMBER = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PRODUCTNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.QUANTITY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PRICE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MONEY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel5 = new System.Windows.Forms.Panel();
            this.txtRefunds = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCustomerPay = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSumMoney = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnOpenTable = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddProduct = new DevExpress.XtraEditors.SimpleButton();
            this.btnPayment = new DevExpress.XtraEditors.SimpleButton();
            this.btnInsertProduct = new DevExpress.XtraEditors.SimpleButton();
            this.btnExit = new DevExpress.XtraEditors.SimpleButton();
            this.btnInsertTable = new DevExpress.XtraEditors.SimpleButton();
            this.btnTrashTable = new DevExpress.XtraEditors.SimpleButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnNameTable = new DevExpress.XtraEditors.SimpleButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvListProduct)).BeginInit();
            this.panel5.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(702, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(451, 923);
            this.panel2.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.grListProduct);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.flowLayoutPanel2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(449, 921);
            this.panel3.TabIndex = 0;
            // 
            // grListProduct
            // 
            this.grListProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListProduct.Location = new System.Drawing.Point(0, 0);
            this.grListProduct.MainView = this.gvListProduct;
            this.grListProduct.Name = "grListProduct";
            this.grListProduct.Size = new System.Drawing.Size(447, 510);
            this.grListProduct.TabIndex = 5;
            this.grListProduct.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvListProduct});
            // 
            // gvListProduct
            // 
            this.gvListProduct.ColumnPanelRowHeight = 70;
            this.gvListProduct.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ROW_NUMBER,
            this.PRODUCTNAME,
            this.QUANTITY,
            this.PRICE,
            this.MONEY});
            this.gvListProduct.GridControl = this.grListProduct;
            this.gvListProduct.Name = "gvListProduct";
            this.gvListProduct.OptionsView.ShowGroupPanel = false;
            this.gvListProduct.RowHeight = 35;
            // 
            // ROW_NUMBER
            // 
            this.ROW_NUMBER.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.ROW_NUMBER.AppearanceCell.Options.UseFont = true;
            this.ROW_NUMBER.AppearanceCell.Options.UseTextOptions = true;
            this.ROW_NUMBER.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ROW_NUMBER.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.ROW_NUMBER.AppearanceHeader.Options.UseFont = true;
            this.ROW_NUMBER.AppearanceHeader.Options.UseTextOptions = true;
            this.ROW_NUMBER.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ROW_NUMBER.Caption = "STT";
            this.ROW_NUMBER.FieldName = "ROW_NUMBER";
            this.ROW_NUMBER.Name = "ROW_NUMBER";
            this.ROW_NUMBER.Visible = true;
            this.ROW_NUMBER.VisibleIndex = 0;
            this.ROW_NUMBER.Width = 55;
            // 
            // PRODUCTNAME
            // 
            this.PRODUCTNAME.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.PRODUCTNAME.AppearanceCell.Options.UseFont = true;
            this.PRODUCTNAME.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.PRODUCTNAME.AppearanceHeader.Options.UseFont = true;
            this.PRODUCTNAME.AppearanceHeader.Options.UseTextOptions = true;
            this.PRODUCTNAME.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PRODUCTNAME.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.PRODUCTNAME.Caption = "Sản phẩm";
            this.PRODUCTNAME.FieldName = "PRODUCTNAME";
            this.PRODUCTNAME.Name = "PRODUCTNAME";
            this.PRODUCTNAME.Visible = true;
            this.PRODUCTNAME.VisibleIndex = 1;
            this.PRODUCTNAME.Width = 96;
            // 
            // QUANTITY
            // 
            this.QUANTITY.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.QUANTITY.AppearanceCell.Options.UseFont = true;
            this.QUANTITY.AppearanceCell.Options.UseTextOptions = true;
            this.QUANTITY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.QUANTITY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.QUANTITY.AppearanceHeader.Options.UseFont = true;
            this.QUANTITY.AppearanceHeader.Options.UseTextOptions = true;
            this.QUANTITY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.QUANTITY.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.QUANTITY.Caption = "Số lượng";
            this.QUANTITY.FieldName = "QUANTITY";
            this.QUANTITY.Name = "QUANTITY";
            this.QUANTITY.Visible = true;
            this.QUANTITY.VisibleIndex = 2;
            this.QUANTITY.Width = 69;
            // 
            // PRICE
            // 
            this.PRICE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.PRICE.AppearanceCell.Options.UseFont = true;
            this.PRICE.AppearanceCell.Options.UseTextOptions = true;
            this.PRICE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.PRICE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.PRICE.AppearanceHeader.Options.UseFont = true;
            this.PRICE.AppearanceHeader.Options.UseTextOptions = true;
            this.PRICE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PRICE.Caption = "Đơn giá";
            this.PRICE.DisplayFormat.FormatString = "{0:#,###}";
            this.PRICE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.PRICE.FieldName = "PRICE";
            this.PRICE.Name = "PRICE";
            this.PRICE.Visible = true;
            this.PRICE.VisibleIndex = 3;
            this.PRICE.Width = 115;
            // 
            // MONEY
            // 
            this.MONEY.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.MONEY.AppearanceCell.Options.UseFont = true;
            this.MONEY.AppearanceCell.Options.UseTextOptions = true;
            this.MONEY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.MONEY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.MONEY.AppearanceHeader.Options.UseFont = true;
            this.MONEY.AppearanceHeader.Options.UseTextOptions = true;
            this.MONEY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.MONEY.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MONEY.Caption = "Thành tiền";
            this.MONEY.DisplayFormat.FormatString = "{0:#,###}";
            this.MONEY.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.MONEY.FieldName = "MONEY";
            this.MONEY.Name = "MONEY";
            this.MONEY.Visible = true;
            this.MONEY.VisibleIndex = 4;
            this.MONEY.Width = 92;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.txtRefunds);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.txtCustomerPay);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.txtSumMoney);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 510);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(447, 188);
            this.panel5.TabIndex = 4;
            // 
            // txtRefunds
            // 
            this.txtRefunds.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRefunds.Location = new System.Drawing.Point(4, 151);
            this.txtRefunds.Name = "txtRefunds";
            this.txtRefunds.Size = new System.Drawing.Size(433, 30);
            this.txtRefunds.TabIndex = 5;
            this.txtRefunds.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(2, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Tiền còn";
            // 
            // txtCustomerPay
            // 
            this.txtCustomerPay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomerPay.Location = new System.Drawing.Point(4, 88);
            this.txtCustomerPay.Name = "txtCustomerPay";
            this.txtCustomerPay.Size = new System.Drawing.Size(433, 30);
            this.txtCustomerPay.TabIndex = 3;
            this.txtCustomerPay.Text = "0";
            this.txtCustomerPay.TextChanged += new System.EventHandler(this.txtCustomerPay_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(2, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tiền khách trả";
            // 
            // txtSumMoney
            // 
            this.txtSumMoney.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSumMoney.Location = new System.Drawing.Point(4, 29);
            this.txtSumMoney.Name = "txtSumMoney";
            this.txtSumMoney.Size = new System.Drawing.Size(433, 30);
            this.txtSumMoney.TabIndex = 1;
            this.txtSumMoney.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(2, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tổng tiền";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.btnOpenTable);
            this.flowLayoutPanel2.Controls.Add(this.btnAddProduct);
            this.flowLayoutPanel2.Controls.Add(this.btnPayment);
            this.flowLayoutPanel2.Controls.Add(this.btnInsertProduct);
            this.flowLayoutPanel2.Controls.Add(this.btnInsertTable);
            this.flowLayoutPanel2.Controls.Add(this.btnTrashTable);
            this.flowLayoutPanel2.Controls.Add(this.btnCancel);
            this.flowLayoutPanel2.Controls.Add(this.btnExit);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 698);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(447, 221);
            this.flowLayoutPanel2.TabIndex = 3;
            // 
            // btnOpenTable
            // 
            this.btnOpenTable.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnOpenTable.Appearance.Options.UseFont = true;
            this.btnOpenTable.Appearance.Options.UseTextOptions = true;
            this.btnOpenTable.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnOpenTable.Location = new System.Drawing.Point(3, 3);
            this.btnOpenTable.Name = "btnOpenTable";
            this.btnOpenTable.Size = new System.Drawing.Size(130, 67);
            this.btnOpenTable.TabIndex = 0;
            this.btnOpenTable.Text = "Mở bàn";
            this.btnOpenTable.Click += new System.EventHandler(this.btnOpenTable_Click);
            // 
            // btnAddProduct
            // 
            this.btnAddProduct.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnAddProduct.Appearance.Options.UseFont = true;
            this.btnAddProduct.Appearance.Options.UseTextOptions = true;
            this.btnAddProduct.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnAddProduct.Location = new System.Drawing.Point(139, 3);
            this.btnAddProduct.Name = "btnAddProduct";
            this.btnAddProduct.Size = new System.Drawing.Size(162, 67);
            this.btnAddProduct.TabIndex = 1;
            this.btnAddProduct.Text = "Thêm món";
            // 
            // btnPayment
            // 
            this.btnPayment.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnPayment.Appearance.Options.UseFont = true;
            this.btnPayment.Appearance.Options.UseTextOptions = true;
            this.btnPayment.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnPayment.Location = new System.Drawing.Point(307, 3);
            this.btnPayment.Name = "btnPayment";
            this.btnPayment.Size = new System.Drawing.Size(130, 67);
            this.btnPayment.TabIndex = 3;
            this.btnPayment.Text = "Thanh toán";
            this.btnPayment.Click += new System.EventHandler(this.btnPayment_Click);
            // 
            // btnInsertProduct
            // 
            this.btnInsertProduct.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnInsertProduct.Appearance.Options.UseFont = true;
            this.btnInsertProduct.Appearance.Options.UseTextOptions = true;
            this.btnInsertProduct.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnInsertProduct.Location = new System.Drawing.Point(3, 76);
            this.btnInsertProduct.Name = "btnInsertProduct";
            this.btnInsertProduct.Size = new System.Drawing.Size(130, 67);
            this.btnInsertProduct.TabIndex = 4;
            this.btnInsertProduct.Text = "Thêm sản phẩm";
            this.btnInsertProduct.Click += new System.EventHandler(this.btnInsertProduct_Click);
            // 
            // btnExit
            // 
            this.btnExit.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnExit.Appearance.Options.UseFont = true;
            this.btnExit.Appearance.Options.UseTextOptions = true;
            this.btnExit.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnExit.Location = new System.Drawing.Point(139, 149);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(162, 67);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "Thoát ứng dụng";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnInsertTable
            // 
            this.btnInsertTable.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnInsertTable.Appearance.Options.UseFont = true;
            this.btnInsertTable.Appearance.Options.UseTextOptions = true;
            this.btnInsertTable.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnInsertTable.Location = new System.Drawing.Point(139, 76);
            this.btnInsertTable.Name = "btnInsertTable";
            this.btnInsertTable.Size = new System.Drawing.Size(162, 67);
            this.btnInsertTable.TabIndex = 6;
            this.btnInsertTable.Text = "Thêm  bàn";
            this.btnInsertTable.Click += new System.EventHandler(this.btnInsertTable_Click);
            // 
            // btnTrashTable
            // 
            this.btnTrashTable.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnTrashTable.Appearance.Options.UseFont = true;
            this.btnTrashTable.Appearance.Options.UseTextOptions = true;
            this.btnTrashTable.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnTrashTable.Location = new System.Drawing.Point(307, 76);
            this.btnTrashTable.Name = "btnTrashTable";
            this.btnTrashTable.Size = new System.Drawing.Size(130, 67);
            this.btnTrashTable.TabIndex = 7;
            this.btnTrashTable.Text = "Xóa bàn";
            this.btnTrashTable.Click += new System.EventHandler(this.btnTrashTable_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(702, 923);
            this.panel1.TabIndex = 2;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Controls.Add(this.panel6);
            this.flowLayoutPanel1.Controls.Add(this.simpleButton1);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(702, 923);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.btnNameTable);
            this.panel6.Controls.Add(this.pictureBox1);
            this.panel6.Location = new System.Drawing.Point(3, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(93, 149);
            this.panel6.TabIndex = 0;
            // 
            // btnNameTable
            // 
            this.btnNameTable.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnNameTable.Appearance.Options.UseFont = true;
            this.btnNameTable.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNameTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNameTable.Location = new System.Drawing.Point(0, 96);
            this.btnNameTable.Name = "btnNameTable";
            this.btnNameTable.Size = new System.Drawing.Size(93, 53);
            this.btnNameTable.TabIndex = 1;
            this.btnNameTable.Text = "Tên bàn";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Image = global::SALE_CAFE.Properties.Resources.cafe;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(93, 96);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Appearance.Options.UseTextOptions = true;
            this.simpleButton1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.simpleButton1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simpleButton1.Location = new System.Drawing.Point(102, 3);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(120, 149);
            this.simpleButton1.TabIndex = 2;
            this.simpleButton1.Text = "tên sản phẩm 25,000";
            // 
            // btnCancel
            // 
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.Appearance.Options.UseTextOptions = true;
            this.btnCancel.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnCancel.Location = new System.Drawing.Point(3, 149);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(130, 67);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Hủy tạo bàn";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // Frm_SaleCafe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1153, 923);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Frm_SaleCafe";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PHẦN MỀM QUẢN LÝ CAFE";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Frm_SaleCafe_Load);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvListProduct)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.SimpleButton btnNameTable;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox txtRefunds;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCustomerPay;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSumMoney;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.SimpleButton btnOpenTable;
        private DevExpress.XtraEditors.SimpleButton btnAddProduct;
        private DevExpress.XtraEditors.SimpleButton btnPayment;
        private DevExpress.XtraEditors.SimpleButton btnInsertProduct;
        private DevExpress.XtraEditors.SimpleButton btnExit;
        private DevExpress.XtraEditors.SimpleButton btnInsertTable;
        private DevExpress.XtraEditors.SimpleButton btnTrashTable;
        private DevExpress.XtraGrid.GridControl grListProduct;
        private DevExpress.XtraGrid.Views.Grid.GridView gvListProduct;
        private DevExpress.XtraGrid.Columns.GridColumn ROW_NUMBER;
        private DevExpress.XtraGrid.Columns.GridColumn PRODUCTNAME;
        private DevExpress.XtraGrid.Columns.GridColumn QUANTITY;
        private DevExpress.XtraGrid.Columns.GridColumn PRICE;
        private DevExpress.XtraGrid.Columns.GridColumn MONEY;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
    }
}

