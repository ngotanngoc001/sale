﻿using Common.Core.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SALE_CAFE
{
    public partial class Frm_Table_Add : Form
    {
        public Frm_Table_Add()
        {
            InitializeComponent();
        }

        private void Frm_Table_Add_Load(object sender, EventArgs e)
        {

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            using (var db = new DatabaseContext())
            {
                try
                {
                    // delete all table
                    db.SqlQuery("[dbo].[TABLE_DELETE]", CommandType.StoredProcedure);

                    for (int i = 1; i <= int.Parse(txtQuantity.Text); i++)
                    {
                        string name = string.Concat(txtName.Text, " - ", i);
                        DataTable dt = db.SqlQueryWithResult("[dbo].[TABLE_INSERT]", CommandType.StoredProcedure,
                       new SqlParameter("@name", name));
                    }

                    Frm_SaleCafe form = new Frm_SaleCafe();
                    form.Frm_SaleCafe_Load(sender, e);
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Lỗi");
                }
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
